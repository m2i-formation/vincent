import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TP {

	static ChromeDriver driver;
	static String varRefPC;
	WebDriverWait wait = new WebDriverWait(driver,10);
	
	@BeforeClass
	public static void setupClass() {
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		
	}

	
	
	@Test
	public void a_Connexion() {
		
		driver.get("https://demo.glpi-project.org/index.php");
		assertTrue(driver.getTitle().contains("Authentication"));
		
		driver.findElement(By.id("login_name")).sendKeys("admin");
		driver.findElement(By.id("login_password")).sendKeys("admin");
		
		new Select(driver.findElement(By.name("language"))).selectByVisibleText("Fran�ais");
		
		driver.findElement(By.name("submit")).click();
		
		assertTrue(driver.findElement(By.id("header_top")).getText().contains("Admin"));
		
	}
	
	@Test
	public void b_AjouterOrdi() {
		
		WebElement Element = driver.findElement(By.linkText("Parc"));
		Actions action = new Actions(driver);
		action.moveToElement(Element).build().perform();
		
		driver.findElement(By.linkText("Ordinateurs")).click();		
		driver.findElement(By.xpath("//a[@title='Ajouter']")).click();
		driver.findElement(By.linkText("Gabarit vide")).click();
		
		varRefPC = "POUET ORDI";
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.name("name")));
		driver.findElement(By.name("name")).sendKeys(varRefPC);
		driver.findElement(By.name("serial")).sendKeys(getRandomReference());
		

		
		driver.findElement(By.name("add")).click();
		
		driver.findElement(By.name("globalsearch")).sendKeys(varRefPC);
		driver.findElement(By.name("globalsearchglass")).click();
		driver.findElement(By.linkText(varRefPC)).click();
		
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.name("name")));
		Assert.assertTrue(driver.findElement(By.tagName("body")).getText().contains("POUET ORDI"));
		
		
	}
	
	
	@Test
	public void c_Deconnexion() {
		
		driver.findElement(By.xpath("//a[@title='D�connexion']")).click();
	}
	
	
	@AfterClass
	public static void tearDownClass() {
		driver.quit();
	}
	
	public String getRandomReference(){
		 String ref="";
		 String allowedChars = "ABCDEFGHIJKLMNOPQRSTUVWYZ";
		 int stringLength = 4;
		 for (int i=0; i<stringLength; i++) {
		        int rnum = (int) Math.floor(Math.random() * allowedChars.length());
		        ref += allowedChars.substring(rnum,rnum+1);
		    }
		    ref += "-";
			for (int i=0; i<stringLength; i++) {
		        int rnum = (int) Math.floor(Math.random() * 9);
		        ref += rnum;
		    }
		 return ref;
	 }
	
}
